const button = document.getElementById("button");

function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}

button.onclick = function () {
    t = document.getElementById("txt").value;

    let solution = [];

    for (let w in words) {
        if (alphabetize(words[w]) === alphabetize(t)) {
            solution.push(words[w]);
        }
    }
    addAnagram(solution);
    console.log(solution);
}

function addAnagram(text) {
    let gramEle = document.createElement("div");
    gramEle.textContent = JSON.stringify(text);
    document.body.appendChild(gramEle);
}